<?php

class Parse
{
    /**
    * @var string
    */
    private $url;

    /**
    * @var array
    */
    private $images;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public static function parse($url)
    {
        (new Parse($url))->parseImages();
    }

    public function parseImages()
    {
        $this->findImages();
        $this->saveImages();
    }

    private function findImages()
    {
        $html = file_get_contents($this->url);
        preg_match_all('/<img.*?src=["\'](.*?)["\'].*?>/i', $html, $this->images, PREG_SET_ORDER);
    }

    private function defineFormat($image): string
    {
        return $format = strtolower(substr(strrchr($image[1], '.'), 1));
    }

    private function supportedFormat($image): bool
    {
        return in_array($this->defineFormat($image), ['jpg', 'jpeg', 'png', 'gif']);
    }

    private function addHttpInImageLink($image): array
    {
        if (mb_substr($image[1], 0, 2) === '//') {
            $image[1] = 'http:' . $image[1];
        }
        return $image;
    }


    private function storage(): string
    {
        $path = dirname(__FILE__) . '/download';
        return $path = rtrim($path, '/');
    }

    public function saveImages()
    {
        $urlImage = [];
        foreach ($this->images as $image) {
            $image = $this->addHttpInImageLink($image);
            if ($this->supportedFormat($image)) {
                $img = parse_url($image[1]);

                $pathImg = $this->storage() . '/' . dirname($img['path']);
                if (!is_dir($pathImg)) {
                    mkdir($pathImg, 0777, true);
                }

                if (empty($img['host']) && !empty($img['path'])) {
                    copy($urlImage['scheme'] . '://' . $urlImage['host'] . $img['path'], $this->storage() . $img['path']);
                }
                copy($image[1], $this->storage() . $img['path']);
            }
        }
    }
}
(new Parse($argv[1]))->parseImages();
//or
Parse::parse($argv[1]);